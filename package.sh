#!/bin/bash
set -o pipefail
set -o errexit
set -o nounset

cd ~/rpm
rpmbuild -ba SPECS/jbdeprpm.spec
rpmbuild -ba SPECS/jbrpm.spec
