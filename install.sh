#!/bin/bash
set -o pipefail
set -o errexit
set -o nounset

cd ~/rpm
rpm -iv RPMS/x86_64/jbrpm-1.0-1.x86_64.rpm RPMS/x86_64/jbdeprpm-1.0-1.x86_64.rpm