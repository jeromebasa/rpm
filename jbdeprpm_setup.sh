#!/bin/bash
set -o pipefail
set -o errexit
set -o nounset

cd ~/rpm
mkdir -p jbdeprpm

echo '#!/bin/bash' > jbdeprpm/jbdeprpm
echo 'echo "jbdeprpm: Hello :)"' >> jbdeprpm/jbdeprpm
chmod +x jbdeprpm/jbdeprpm
echo '# jbdeprpm.conf' > jbdeprpm/jbdeprpm.conf

mkdir -p jbdeprpm-1.0
mkdir -p jbdeprpm-1.0/usr/bin
mkdir -p jbdeprpm-1.0/etc/jbdeprpm

install -m 755 jbdeprpm/jbdeprpm jbdeprpm-1.0/usr/bin/
install -m 644 jbdeprpm/jbdeprpm.conf jbdeprpm-1.0/etc/jbdeprpm/

tar -zcvf SOURCES/jbdeprpm-1.0.tar.gz jbdeprpm-1.0/