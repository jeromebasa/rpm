# RPM

Sample RPM package. Make sure that `rpm-build` package is installed.

1. Clone in `~/rpm` dir.
1. Run `init.sh`.
1. Run `jbrpm_setup.sh` and `jbdeprpm_setup.sh`
1. Copy `SPECS/*` to `~/rpm/SPECS/`.
1. Run `package.sh`.
1. Run `install.sh` (should be run as root).

To uninstall, run `uninstall.sh`.

## Test Log

```text
[root@li1327-113 repo]# ./install.sh
Preparing packages...
---------> pre-install script.
jbdeprpm-1.0-1.x86_64
---------> post-install script.
---------> pre-install script.
jbrpm-1.0-1.x86_64
---------> post-install script.
---------> trigger install script

[root@li1327-113 repo]# rpm -q jbrpm
jbrpm-1.0-1.x86_64
[root@li1327-113 repo]# rpm -ql jbrpm
/etc/jbrpm/jbrpm.conf
/usr/bin/jbrpm

[root@li1327-113 repo]# rpm -q jbdeprpm
jbdeprpm-1.0-1.x86_64
[root@li1327-113 repo]# rpm -ql jbdeprpm
/etc/jbdeprpm/jbdeprpm.conf
/usr/bin/jbdeprpm

[root@li1327-113 repo]# /usr/bin/jbrpm
JBRPM: Hello :)
[root@li1327-113 repo]# /usr/bin/jbdeprpm
jbdeprpm: Hello :)

[root@li1327-113 repo]# ./uninstall.sh
Preparing packages...
---------> trigger uninstall script
---------> pre-uninstall script
jbrpm-1.0-1.x86_64
---------> post-uninstall script
---------> pre-uninstall script
jbdeprpm-1.0-1.x86_64
---------> post-uninstall script

[root@li1327-113 repo]# rpm -iv ~/rpm/RPMS/x86_64/jbrpm-1.0-1.x86_64.rpm
error: Failed dependencies:
  jbdeprpm is needed by jbrpm-1.0-1.x86_64
```
