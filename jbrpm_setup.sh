#!/bin/bash
set -o pipefail
set -o errexit
set -o nounset

cd ~/rpm
mkdir -p jbrpm

echo '#!/bin/bash' > jbrpm/jbrpm
echo 'echo "JBRPM: Hello :)"' >> jbrpm/jbrpm
chmod +x jbrpm/jbrpm
echo '# jbrpm.conf' > jbrpm/jbrpm.conf

mkdir -p jbrpm-1.0
mkdir -p jbrpm-1.0/usr/bin
mkdir -p jbrpm-1.0/etc/jbrpm

install -m 755 jbrpm/jbrpm jbrpm-1.0/usr/bin/
install -m 644 jbrpm/jbrpm.conf jbrpm-1.0/etc/jbrpm/

tar -zcvf SOURCES/jbrpm-1.0.tar.gz jbrpm-1.0/