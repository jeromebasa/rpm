#!/bin/bash
set -o pipefail
set -o errexit
set -o nounset

mkdir -p ~/rpm/{RPMS,SRPMS,BUILD,SOURCES,SPECS,tmp}

cat <<EOF >~/.rpmmacros
%_topdir   %(echo $HOME)/rpm
%_tmppath  %{_topdir}/tmp
EOF