%define        __spec_install_post %{nil}
%define          debug_package %{nil}
%define        __os_install_post %{_dbpath}/brp-compress

Summary: Test RPM dependency package
Name: jbdeprpm
Version: 1.0
Release: 1
License: MIT
Group: Development/Tools
SOURCE0 : %{name}-%{version}.tar.gz
URL: http://jeromebasa.com

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root

%description
%{summary}

%prep
%setup -q

%build
# Empty section.

%clean
rm -rf %{buildroot}

%pre
echo '---------> pre-install script.'

%install
rm -rf %{buildroot}
mkdir -p  %{buildroot}

# in builddir
cp -a * %{buildroot}

%post
echo '---------> post-install script.'

%preun
echo '---------> pre-uninstall script'

%postun
echo '---------> post-uninstall script'

%files
%defattr(-,root,root,-)
%config(noreplace) %{_sysconfdir}/%{name}/%{name}.conf
%{_bindir}/*

%changelog
* Fri Mar 20 2020 Jerome Basa <jeromebasa@gmail.com> 1.0-1
- First build